package com.victorroiget.technicaltest;

import com.victorroiget.beans.animales.Animal;
import com.victorroiget.beans.animales.AnimalObserved;
import com.victorroiget.beans.animales.AnimalUtils;
import com.victorroiget.beans.animales.IObserver;
import com.victorroiget.beans.animales.impl.Perro;
import com.victorroiget.beans.animales.impl.dog.Type;
import com.victorroiget.beans.animales.impl.pajaros_impl.Loro;
import com.victorroiget.beans.animales.impl.pajaros_impl.Pollo;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Classe main.
 *
 * Contiene el main, la lista de Animales el menu y el propio main.
 *
 * @see IObserver
 */
public class App implements IObserver {
    private List<Animal> animales = Arrays.asList(
            new Perro("pepe", "asdf", Type.caza),
            new Loro("asdf", "asdf", (float) 4.3, true),
            new Pollo("sadfsd", "asdf", (float) 9.0, false)
    ); //Animales hardcodeados para la prueva

    public static void main(String[] args) {
        App app = new App();
        app.menu();
        app.preDestroy();
    }

    public App() { // Nos añadimos a la lista de observers
        AnimalObserved.getInstance().addObserver(this);
    }

    /**
     * Menu para controlar las acciones y obtener el resultado
     */
    public void menu() {
        Scanner scanner = new Scanner(System.in); //Scanner para introducir las opciones por consola
        int option = -1;

        do { //Menu para controlar las opciones
            System.out.println( "\n \n" +
                            "1 - Lista de animales. \n" +
                            "2 - Lista de animales (pretty). \n" +
                            "3 - Vive un dia. \n" +
                            "0 - Salir. \n"
            );

            System.out.print("Por favor seleccione una opción:");
            try {
                option = scanner.nextInt(); //Recogemos la opcion que introduce el usuario
            }catch (InputMismatchException e) {
                System.out.println("Se esperava un numero, por favor introduzca una opcion correcta");
                scanner.nextLine(); //Descargamos el buffer
                continue;// nos saltamos el resto de codigo y reiniciamos el menu
            }

            switch (option) {
                case 1:
                    imprimirLista();
                    break;
                case 2:
                    imprimirLista(true);
                    break;
                case 3:
                    AnimalUtils.liveOneDay(this.animales);
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Esta opcion no existe por favor elija una que si");
            }

        } while (option != 0);
    }

    private void imprimirLista(){
        imprimirLista(false);
    }

    /**
     * Metodo encargado de imprimir por pantall la lista de animales
     * @param pretty
     */
    private void imprimirLista(boolean pretty){
        for (Animal animal : animales)
            System.out.println(animal.toString().replaceAll("//", ((pretty)?"\n \t":", ")));
    }

    public void newOperation(String mensage) {
        System.out.println(mensage);
    }

    /**
     * Metodo encargado de preparar la clesa para ser eliminada
     */
    public void preDestroy() {
        AnimalObserved.getInstance().removeObserver(this);
    }
}
