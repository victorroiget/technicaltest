package com.victorroiget.beans.animales.impl;

import com.victorroiget.beans.animales.Animal;
import com.victorroiget.beans.animales.impl.dog.Type;

/**
 * Classe bean
 *
 * @see Animal
 * @see Type
 */
public class Perro extends Animal {

    private Type tipo = null;

    /**
     * Constructor completo
     * @param nobre
     * @param comidaFavorita
     * @param tipo
     */
    public Perro(String nobre, String comidaFavorita, Type tipo) {
        super(nobre, comidaFavorita);
        this.tipo = tipo;
    }

    /**
     * Constructor simple
     * @param nobre
     */
    public Perro(String nobre) {
        super(nobre);
    }

    /*
    Getters & Setters -----------------------------------------------------------------
     */

    /**
     * Getter de Tipo
     * @return
     */
    public Type getTipo() {
        return tipo;
    }

    /**
     * Setter de tipo
     * @param tipo
     */
    public void setTipo(Type tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return super.toString() + "//tipo: " + this.tipo.toString();
    }
}
