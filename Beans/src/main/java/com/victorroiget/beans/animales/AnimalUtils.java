package com.victorroiget.beans.animales;

import java.util.List;

/**
 * Classe util para los Animal
 *
 * see @{@link Animal}
 */
public class AnimalUtils {

    /**
     * Metodo encargado de crear y destruir relaciones de amistad entre animales de manera aleatoria
     * @param animales
     */
    /*
     *
     * He decidido utilizar el HashCode ya que me parece una manera elegante y sencilla de referenciar de manera
     * unica a un objeto.
     *
     */
    public static void liveOneDay(List<Animal> animales) {
        if (animales.size() < 2) return; // todo decidir si lo pongo
        for (Animal animal : animales) {
            int opcion = -1;
            do {
                opcion = (int) (Math.random() * 5);

                /*
                Aquí segun los resultados del random nos dan las siguientes funciones:
                0- salir
                1- operacion y salir
                2- operacion y continuar

                Donde operación es comprobar si todavia no son amigos, en cuyo caso se hacen amigos, de lo
                contrario rompen la amistad


                Otra opcion huviese sido:
                -0 salir
                -1 añadir amigo y salir
                -2 añadir amigo y continuar
                -3 eliminar amigo y salir
                -4 eliminar amigo y continuar
                 */

                switch (opcion) {
                    case 0:
                        break;
                    case 1:
                    case 2:
                        Animal amigo = null;
                        do {
                                amigo = animales.get(
                                    (int)(Math.random() * animales.size()));
                        } while (amigo.equals(animal));

                        int amigoHashCode = System.identityHashCode(amigo);

                        if (!animal.getAmigos().contains(amigoHashCode)) {
                            animal.getAmigos().add(amigoHashCode);
                            amigo.getAmigos().add(
                                    System.identityHashCode(animal));
                            AnimalObserved.getInstance().sendMessage(animal.getNombre() + " y " + amigo.getNombre() + " se han echo amigos.");
                        } else {
                            /*
                            A continuación le mando los hash como integer para que
                            entre en el metodo con parametro object en lugar del index.
                             */
                            animal.getAmigos().remove((Integer) amigoHashCode);
                            amigo.getAmigos().remove(
                                    (Integer) System.identityHashCode(animal));
                            AnimalObserved.getInstance().sendMessage(animal.getNombre() + " y " + amigo.getNombre() + " ya no son amigos.");
                        }
                        break;
                }
            }while ((opcion != 0) && (opcion % 2 == 0)); // Esta linea tambien serviria en caso de tener las 5 opciones
        }
    }
}
