package com.victorroiget.beans.animales.impl.pajaros_impl;

import com.victorroiget.beans.animales.impl.Pajaro;

/**
 * Especificaciión de pajaro
 *
 * @see Pajaro
 */
public class Pollo extends Pajaro {
    private boolean paraCocinar = false;

    /**
     * Constructor completo
     * @param nobre
     * @param comidaFavorita
     * @param longitudAlas
     * @param paraCocinar
     */
    public Pollo(String nobre, String comidaFavorita, float longitudAlas, boolean paraCocinar) {
        super(nobre, comidaFavorita, longitudAlas);
        this.paraCocinar = paraCocinar;
    }

    /**
     * Constructor simple
     * @param nobre
     */
    public Pollo(String nobre) {
        super(nobre);
    }

    /*
    Getters & Setters -----------------------------------------------------------------
     */

    /**
     * Getter de ParaCocinar
     * @return
     */
    public boolean isParaCocinar() {
        return paraCocinar;
    }

    /**
     * Setter de ParaCocinar
     * @param paraCocinar
     */
    public void setParaCocinar(boolean paraCocinar) {
        this.paraCocinar = paraCocinar;
    }

    @Override
    public String toString() {
        return super.toString() + "//¿Para cocinar? " + ((this.paraCocinar)?"Si":"No");
    }
}
