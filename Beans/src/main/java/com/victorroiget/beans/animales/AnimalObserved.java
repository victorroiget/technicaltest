package com.victorroiget.beans.animales;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe singelton encargada de notificar canvios con los animales
 *
 * @see Animal
 * @see IObserver
 */
public class AnimalObserved {
    private static AnimalObserved instance = null;
    private List<IObserver> observers = null;

    /**
     * Constructor privado para el singelton y encargado de crear el ArrayList
     */
    private AnimalObserved() {
        super();
        observers = new ArrayList<IObserver>();
    }

    /**
     * Metodo para añadir un observer a la lista
     * @param observer
     */
    public void addObserver(IObserver observer) {
        this.observers.add(observer);
    }

    /**
     * Metodo para elimminar un observer de la lista
     * @param observer
     */
    public void removeObserver(IObserver observer) {
        this.observers.remove(observer);
    }

    /**
     * Metodo encargado de notificar
     * @param mensage
     */
    public void sendMessage(String mensage) {
        for (IObserver observer : observers)
            try {
                observer.newOperation(mensage);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    /**
     * Este metodo es el que nos permite obtener la instancia unica de este objeto
     * @return
     */
    public static AnimalObserved getInstance() {
        if (instance == null)
            instance = new AnimalObserved();
        return instance;
    }
}
