package com.victorroiget.beans.animales;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Interface para que las clases que lo deseen recivan las 'notificaciones'
 */
/*
Lo queria hacer con PropertyChangeListener, el problema era que los atributos de PropertyChangeEvent no me
convencian para un caso tan sencillo
 */
public interface IObserver {
    void newOperation(String mensage);
}
