package com.victorroiget.beans.animales.impl;

import com.victorroiget.beans.animales.Animal;

/**
 * Abstract class bean para extender de Animal con los atributos comunes de los pajaros
 *
 * @see Animal
 */
public abstract class Pajaro extends Animal {
    private float longitudAlas = 0;

    /**
     * Constructor completo
     * @param nobre
     * @param comidaFavorita
     * @param longitudAlas
     */
    public Pajaro(String nobre, String comidaFavorita, float longitudAlas) {
        super(nobre, comidaFavorita);
        this.longitudAlas = longitudAlas;
    }

    /**
     * Constructor simple
     * @param nobre
     */
    public Pajaro(String nobre) {
        super(nobre);
    }

    /*
    Getters & Setters -----------------------------------------------------------------
     */

    /**
     * Getter de LongitudAlas
     * @return
     */
    public float getLongitudAlas() {
        return longitudAlas;
    }

    /**
     * Setter de LongitudAlas
     * @param longitudAlas
     */
    public void setLongitudAlas(float longitudAlas) {
        this.longitudAlas = longitudAlas;
    }

    @Override
    public String toString() {
        return super.toString() + "//longitud de las alas: " + longitudAlas;
    }
}
