package com.victorroiget.beans.animales.impl.dog;

/**
 * Enum para los tipos de perro
 */
public enum Type {
    caza,
    trabajo,
    policia
}
