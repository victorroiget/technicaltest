package com.victorroiget.beans.animales.impl.pajaros_impl;

import com.victorroiget.beans.animales.impl.Pajaro;

/**
 * Especificaciión de pajaro
 *
 * @see Pajaro
 */
public class Loro extends Pajaro {
    private boolean puedeHablar = false;

    /**
     * Constructor completo
     * @param nobre
     * @param comidaFavorita
     * @param longitudAlas
     * @param puedeHablar
     */
    public Loro(String nobre, String comidaFavorita, float longitudAlas, boolean puedeHablar) {
        super(nobre, comidaFavorita, longitudAlas);
        this.puedeHablar = puedeHablar;
    }

    /**
     * Constructor simple
     * @param nobre
     */
    public Loro(String nobre) {
        super(nobre);
    }

    /*
    Getters & Setters -----------------------------------------------------------------
     */

    /**
     * Getter de PuedeHablar
     * @return
     */
    public boolean isPuedeHablar() {
        return puedeHablar;
    }

    /**
     * Setter de PuedeHablar
     * @param puedeHablar
     */
    public void setPuedeHablar(boolean puedeHablar) {
        this.puedeHablar = puedeHablar;
    }

    @Override
    public String toString() {
        return super.toString() + "//¿puede hablar? " + ((puedeHablar)?"Si":"No");
    }
}
