package com.victorroiget.beans.animales;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class bean para los atributos comunes de los animales y parte de el toString()
 */
public abstract class Animal {
    private String nombre = null;
    private String comidaFavorita = null;
    private List<Integer> amigos = new ArrayList<Integer>();

    /**
     * Constructor completo
     * @param nombre
     * @param comidaFavorita
     */
    public Animal(String nombre, String comidaFavorita) {
        this.nombre = nombre;
        this.comidaFavorita = comidaFavorita;
    }

    /**
     * Constructor simple
     * @param nobre
     */
    public Animal(String nobre) {
        this.nombre = nobre; //Considero que es importante que un animal tenga nombre, por ello no pongo un constructor vacio
    }

    /*
    Getters & Setters -----------------------------------------------------------------
     */

    /**
     * Getter de nombre
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Setter de nobre
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Cetter de ComidaFvorita
     * @return
     */
    public String getComidaFavorita() {
        return comidaFavorita;
    }

    /**
     * Setter de ComidaFavorita
     * @param comidaFavorita
     */
    public void setComidaFavorita(String comidaFavorita) {
        this.comidaFavorita = comidaFavorita;
    }

    /**
     * Getter de Amigos
     * @return
     */
    public List<Integer> getAmigos() {
        return amigos;
    }

    /**
     * Setter de Amigos
     * @param amigos
     */
    public void setAmigos(List<Integer> amigos) {
        this.amigos = amigos;
    }

    /**
     * Metodo que devuelbe la información del objeto en formato String.
     *
     * A falta de pocesar
     *
     * @return
     */
    @Override
    public String toString() {
        return "Nombre: " + this.nombre + "//Comida favorita: " + comidaFavorita;
    }
}
